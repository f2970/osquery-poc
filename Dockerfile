FROM debian:bullseye

RUN apt-get update && apt-get -y install gnupg software-properties-common
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 1484120AC4E9F8A1A577AEEE97A80C63C9D8B80B
RUN add-apt-repository 'deb [arch=amd64] https://pkg.osquery.io/deb deb main'
RUN apt-get -y -qq update
RUN apt-get install osquery
