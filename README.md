# Introduction
This repository adds a Dockerfile and a docker-compose file  
To run it, simply run `docker compose up -d`  
It provides an empty debian instance with osquery installed  

# PoC
Open osquery via `osqueryi` and use the following query:
```sql
SELECT process.name, listening.port, process.pid FROM processes AS process JOIN listening_ports AS listening ON process.pid = listening.pid WHERE listening.address = '0.0.0.0' ORDER BY 'process.name';
```

You will see no output. Exit osquery now and run 
```sh
$ apt install nginx
$ service nginx start
```

Run osquery again via `osqueryi` and execute the same SELECT query as above.

The output should look something like:
```
root@d80133b216c7:/# osqueryi
Using a virtual database. Need help, type '.help'
osquery> SELECT process.name, listening.port, process.pid FROM processes AS process JOIN listening_ports AS listening ON process.pid = listening.pid WHERE listening.address = '0.0.0.0' ORDER BY 'process.name';
+-------+------+-----+
| name  | port | pid |
+-------+------+-----+
| nginx | 80   | 729 |
| nginx | 80   | 729 |
+-------+------+-----+
osquery>
```

This is only very basic what osquery can fetch from your system. Combine it with a tool in the likes of [Fleet DM](https://fleetdm.com/) to send system logs to any location configured by a sysadmin and it becomes a powerful monitoring tool
